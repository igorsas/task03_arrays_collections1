package com.igor.Containers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import static java.util.Arrays.sort;

public class ContainerExample {
    public static void main(String[] args) {
        StringsContainer container = new StringsContainer(1000);
        List<String> list = new ArrayList<>(1000);
        compareAdding(container, list);
        compareGetting(container, list);

        Scanner scanner = new Scanner(System.in);
        FullName[] people = new FullName[5];
        List<FullName> arrayPeople = new ArrayList<>();
        String firstName, lastName;
        for (int i = 0; i < 5; i++) {
            System.out.println("Enter first name for people №" + i);
            firstName = scanner.nextLine();
            System.out.println("Enter last name for people №" + i);
            lastName = scanner.nextLine();
            FullName person = new FullName(firstName, lastName);
            people[i] = person;
            arrayPeople.add(i, person);
        }
        printFromArray(people);
        printFromArrayList(arrayPeople);
        System.out.println("\nSorting by first name");
        sort(people);
        arrayPeople.sort(idComparator);
        printFromArray(people);
        printFromArrayList(arrayPeople);
    }

    //choose here which method for compare to object of class FullName use
    public static Comparator<FullName> idComparator = FullName::compareToLast;

    private static void printFromArray(FullName[] people){
        System.out.println("People in array:");
        for (FullName person : people){
            System.out.println(person);
        }
    }

    private static void printFromArrayList(List<FullName> arrayPeople){
        System.out.println("\nPeople in ArrayList:");
        for (FullName person : arrayPeople){
            System.out.println(person);
        }

    }

    private static void compareAdding(StringsContainer container, List<String> list){
        long start, end;
        start = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            container.add(String.valueOf(i));
        }
        end = System.nanoTime();
        System.out.println("The time of adding 1000 elements in my container: " + (end - start) + "ns");

        start = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            list.add(String.valueOf(i));
        }
        end = System.nanoTime();
        System.out.println("The time of adding 1000 elements in ArrayList: " + (end - start) + "ns");
    }

    private static void compareGetting(StringsContainer container, List<String> list){
        long start, end;
        start = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            container.get(i);
        }
        end = System.nanoTime();
        System.out.println("The time of getting 1000 elements in my container: " + (end - start) + "ns");

        start = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            list.get(i);
        }
        end = System.nanoTime();
        System.out.println("The time of getting 1000 elements in ArrayList: " + (end - start) + "ns");
    }
}
