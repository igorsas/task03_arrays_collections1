package com.igor.Containers;

public class FullName implements Comparable{
    private String firstName;
    private String lastName;

    @Override
    public String toString() {
        return "FullName{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    public FullName(String firstName, String lastName){
        if(firstName.equals("") || firstName.isEmpty() ||
                lastName.equals("") || lastName.isEmpty()){
            throw new NullPointerException();
        }
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int compareToFirst(Object o) {
        return this.firstName.compareTo(((FullName)o).firstName);
    }

    public int compareToLast(Object o){
        return this.lastName.compareTo(((FullName)o).lastName);
    }

    @Override
    public int compareTo(Object o) {
        return compareToFirst(o);
    }
}
