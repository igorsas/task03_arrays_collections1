package com.igor.Containers;

public class StringsContainer {
    private String[] array;

    public StringsContainer(int size){
        array = new String[size];
    }

    public StringsContainer(String[] array){
        this.array = array;
    }

    public String get(int index){
        return array[index];
    }

    public void add(String s){
        String[] array = new String[this.array.length + 1];
        for (int i = 0; i < this.array.length; i++) {
            array[i] = this.array[i];
        }
        array[array.length-1] = s;
    }
}
