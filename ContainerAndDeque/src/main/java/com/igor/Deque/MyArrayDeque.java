package com.igor.Deque;

import java.util.*;

public class MyArrayDeque<E> implements Deque {
    private ArrayList<E> deque;
    private Comparator<? super E> comparator;


    public MyArrayDeque(){
        deque = new ArrayList<>();

    }

    public MyArrayDeque(int initialCapacity) {
        deque = new ArrayList<>(initialCapacity);
    }

    public MyArrayDeque(int initialCapacity,
                         Comparator<? super E> comparator) {
        if (initialCapacity < 1)
            throw new IllegalArgumentException();
        this.deque = new ArrayList<>(initialCapacity);
        this.comparator = comparator;
    }

    public MyArrayDeque(MyArrayDeque<? extends E> c) {
        this.comparator = (Comparator<? super E>) c.comparator();
        this.deque = new ArrayList<>(c);
    }

    public void clear(){
        deque = new ArrayList<>();
    }

    @Override
    public boolean equals(Object o) {
        return false;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    public Comparator<? super E> comparator(){
        return comparator;
    }

    public boolean contains(Object o){
        for(E element : deque){
            if(element.equals(o)){
                return true;
            }
        }
        return false;
    }

    public Iterator<E> iterator(){
        return new IteratorDeque();
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    @Override
    public Iterator descendingIterator() {
        return null;
    }

    @Override
    public E peek(){
        if(deque.isEmpty()){
            return null;
        }else {
            return deque.get(0);
        }
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public void push(Object o) {
        addLast(o);
    }

    @Override
    public E pop() {
        return removeLast();
    }

    @Override
    public void addFirst(Object o) {
        offerFirst(o);
    }

    @Override
    public void addLast(Object o) {
        offerLast(o);
    }

    @Override
    public boolean offerFirst(Object o) {
        return offer(o);
    }

    @Override
    public boolean offerLast(Object o) {
        if (o == null)
            throw new NullPointerException();
        List<E> list = new ArrayList<>(deque.size() + 1);
        list.set(0, (E) o);
        for (int i = 0; i < deque.size(); i++) {
            list.set(i+1, deque.get(i));
        }
        return true;
    }

    @Override
    public E removeFirst() {
        return remove();
    }

    @Override
    public E removeLast() {
        E e = deque.get(deque.size()-1);
        deque.remove(e);
        return e;
    }

    @Override
    public E pollFirst() {
        return poll();
    }

    @Override
    public E pollLast() {
        if(deque.isEmpty()){
            return null;
        }else {
            E o =  deque.get(deque.size()-1);
            deque.remove(deque.size()-1);
            return o;
        }
    }

    @Override
    public E getFirst() {
        return peekFirst();
    }

    @Override
    public E getLast() {
        return peekLast();
    }

    @Override
    public E peekFirst() {
        return peek();
    }

    @Override
    public E peekLast() {
        if(deque.isEmpty()){
            return null;
        }else {
            return deque.get(deque.size()-1);
        }
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        return remove(o);
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        for (int i = deque.size()-1; i >= 0; i--) {
            if(deque.get(i).equals(o)){
                return deque.remove(i) != null;
            }
        }
        return false;
    }

    @Override
    public boolean add(Object o) {
        return offer(o);
    }

    @Override
    public boolean offer(Object o) {
        if (o == null)
            throw new NullPointerException();
        List<E> list = new ArrayList<>(deque.size() + 1);
        for (int i = 0; i < deque.size(); i++) {
            list.set(i, deque.get(i));
        }
        return list.set(list.size() - 1, (E) o) != null;
    }

    @Override
    public E remove() {
        E e = deque.get(0);
        deque.remove(e);
        return e;
    }

    @Override
    public E poll(){
        if(deque.isEmpty()){
            return null;
        }else {
            E o =  deque.get(0);
            deque.remove(0);
            return o;
        }
    }

    @Override
    public Object element() {
        return deque.get(0);
    }

    @Override
    public boolean remove(Object o){
        return deque.remove(o);
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public int size(){
        return deque.size();
    }

    @Override
    public boolean isEmpty() {
        return deque.isEmpty();
    }

    public ArrayList<E> getArray(){
        return deque;
    }

    private class IteratorDeque implements Iterator<E>{
        private int index = 0;
        @Override
        public boolean hasNext() {
            return deque.get(index).equals(null) ? false : true;
        }

        @Override
        public E next() {
            return deque.get(index++);
        }

        @Override
        public void remove() {
            deque.remove(index);
        }
    }
}
