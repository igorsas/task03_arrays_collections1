package com.igor;

import java.util.Scanner;

public class A {
    public static void main(String[] args) {
        int[] array0 = inputArray();
        int[] array1 = inputArray();
        System.out.print("Array0: ");
        printArray(array0);
        System.out.print("\nArray1: ");
        printArray(array1);

        int[] array2 = a(array0, array1);
        System.out.print("\nArray2: ");
        printArray(array2);

        int[] array3 = b(array0, array1);
        System.out.print("\nArray3: ");
        printArray(array3);
    }

    public static void printArray(int[] array){
        for (int i : array){
            System.out.print(i + " ");
        }
    }

    public static int[] inputArray(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter size of array: ");
        int size = scanner.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            System.out.println("Input element №" + i);
            array[i] = scanner.nextInt();
        }
        return array;
    }

    private static int[] a(int[] array0, int[] array1){
        int size = 0;
        ArraySize: for (int i = 0; i < array0.length; i++) {
            for (int j = 0; j < i; j++) {
                if(array0[j] == array0[i]){
                    continue ArraySize;
                }
            }
            for (int j = 0; j < array1.length; j++) {
                if(array0[i] == array1[j]){
                    size++;
                    break;
                }
            }
        }
        int[] array = new int[size];
        int index = 0;
        ArrayElement: for(int i = 0; i < array0.length; i++) {
            for (int j = 0; j < i; j++) {
                if(array0[j] == array0[i]){
                    continue ArrayElement;
                }
            }
            for (int j = 0; j < array1.length; j++) {
                if(array0[i] == array1[j]){
                    array[index] = array0[i];
                    index++;
                    break;
                }
            }
        }
        return array;
    }

    private static int[] b(int[] array0, int[] array1){
        int size = size(array0, array1) + size(array1, array0);
        int[] array = new int[size];
        int index = 0;
        index = fillArray(array, array0, array1, index);
        fillArray(array, array1, array0, index);
        return array;
    }

    public static int size(int[] array0, int[] array1){
        int size = 0;
        ArraySize: for (int i = 0; i < array0.length; i++) {
            for (int j = 0; j < i; j++) {
                if(array0[j] == array0[i]){
                    continue ArraySize;
                }
            }
            for (int j = 0; j < array1.length; j++) {
                if(array0[i] == array1[j]){
                    continue ArraySize;
                }
            }
            size++;
        }
        return size;
    }

    public static int fillArray(int[] array, int[] array0, int[] array1, int index){
        ArrayElement: for(int i = 0; i < array0.length; i++) {
            for (int j = 0; j < i; j++) {
                if(array0[j] == array0[i]){
                    continue ArrayElement;
                }
            }
            for (int j = 0; j < array1.length; j++) {
                if(array0[i] == array1[j]){
                    continue ArrayElement;
                }
            }
            array[index] = array0[i];
            index++;
        }
        return index;
    }
}
