package com.igor;

import static java.util.Arrays.sort;

public class B {
    public static void main(String[] args) {
        int[] array = A.inputArray();
        System.out.print("Original array: ");
        A.printArray(array);
        sort(array);
        int size = 0;
        for (int i = 0; i < array.length; i++) {
            int count = findNumberOfReplace(array, i);
            if(count > 2){
                size++;
                i+=count-1;
            }
        }

        int[] arrayResult = new int[size];
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            int count = findNumberOfReplace(array, i);
            if(count > 2){
                arrayResult[index] = array[i];
                index++;
                i+=count-1;
            }
        }
        System.out.print("\nResult array: ");
        A.printArray(arrayResult);
    }

    public static int findNumberOfReplace(int[] array, int i) {
        int count = 1;
        for (int j = i+1; j < array.length; j++) {
            if(array[i] == array[j]){
                count++;
            }else {
                break;
            }
        }
        return count;
    }
}
