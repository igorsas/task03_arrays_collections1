package com.igor;

public class C {
    public static void main(String[] args) {
        int[] array = A.inputArray();
        System.out.print("Original array: ");
        A.printArray(array);
        int size = array.length;
        for (int i = 0; i < array.length; i++) {
            int count = B.findNumberOfReplace(array, i);
            if(count > 1){
                size-=count-1;
                i+=count-1;
            }
        }

        int[] arrayResult = new int[size];
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            int count = B.findNumberOfReplace(array, i);
            arrayResult[index] = array[i];
            index++;
            if(count > 1){
                i+=count-1;
            }
        }
        System.out.print("\nResult array: ");
        A.printArray(arrayResult);
    }
}
