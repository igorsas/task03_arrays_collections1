package com.igor;

import java.util.Random;

import static java.util.Arrays.sort;

public class D {
    public static void main(String[] args) {
        int heroPoints = 25;
        int[] doors = fillDoors(10);
        System.out.println("Doors:\n");
        printDoors(doors);
        System.out.println("\nThe best way: " + findBetterWay(doors, heroPoints));

    }
    private static void printDoors(int[] doors){
        for (int i = 0; i < doors.length; i++) {
            if(doors[i] >= 10){
                System.out.println(i + ". Here is artifact with " + doors[i] + " points");
            }else {
                System.out.println(i + ". Here is monster with power " + (doors[i]*(-1)) + " points");
            }
        }
    }

    private static int[] fillDoors(int count) {
        Random rand = new Random();
        int[] doors = new int[count];
        for (int i = 0; i < count; i++) {
            int randPoints = rand.nextInt(180) - 100;
            if((randPoints >= 10 && randPoints <= 80) || (randPoints <= -5 && randPoints >= -100)){
                doors[i] = randPoints;
            }else{
                doors[i] = 10;
            }
        }
        return doors;
    }
    /*
    I can't return 2 values (steps and heroPoints)
     */
    private static String findBetterWay(int[] doors, int heroPoints){
        String steps =  new String("");
        for (int i = 0; i < doors.length; i++) {
            if(doors[i] >= 10){
                steps += "\nYou should choose door №" + i + "\nHere you'll keep artifact.";
                heroPoints += doors[i];
                doors[i] = 0;
            }else if(doors[i] <= -5){
                if(heroPoints + doors[i] >= 0){
                    steps += "\nYou should choose door №" + i + "\nHere you'll kill monster.";
                    doors[i] = 0;
                }
            }
        }
        if(checkAllDoors(doors)){
            for (int i = 0; i < doors.length; i++) {
                if(doors[i] >= 10){
                    steps += "\nYou should choose door №" + i + "\nHere you'll keep artifact.";
                    heroPoints += doors[i];
                    doors[i] = 0;
                }else if(doors[i] <= -5){
                    if(heroPoints + doors[i] >= 0){
                        steps += "\nYou should choose door №" + i + "\nHere you'll kill monster.";
                        doors[i] = 0;
                    }
                }
            }
            if(checkAllDoors(doors)){
                return "You can't win in this game.";
            }else {
                return steps;
            }
        }
        return steps;
    }

    private static boolean checkAllDoors(int[] doors){
        for (int door : doors){
            if(door != 0){
                return true;
            }
        }
        return false;
    }
}
