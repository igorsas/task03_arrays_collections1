package com.igor;

import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.function.Consumer;

public class PriorityQueue<E>  extends AbstractQueue<E> {
    private ArrayList<E> queue;
    private Comparator<? super E> comparator;


    public PriorityQueue(){
        queue = new ArrayList<>();

    }

    public PriorityQueue(int initialCapacity) {
        queue = new ArrayList<>(initialCapacity);
    }

    public PriorityQueue(int initialCapacity,
                         Comparator<? super E> comparator) {
        if (initialCapacity < 1)
            throw new IllegalArgumentException();
        this.queue = new ArrayList<>(initialCapacity);
        this.comparator = comparator;
    }

    public PriorityQueue(PriorityQueue<? extends E> c) {
        this.comparator = (Comparator<? super E>) c.comparator();
        this.queue = new ArrayList<>(c);
    }


    public boolean add(E e){
        return offer(e);
    }

    public void clear(){
        queue = new ArrayList<>();
    }

    public Comparator<? super E> comparator(){
        return comparator;
    }

    public boolean contains(Object o){
        for(E element : queue){
            if(element.equals(o)){
                return true;
            }
        }
        return false;
    }

    public Iterator<E> iterator(){
        return new IteratorQueue();
    }

    public boolean offer(E e){
        if (e == null)
            throw new NullPointerException();
        int i = queue.size();
        if (i == 0)
            queue.add(e);
        else
            siftUp(i, e);
        return true;
    }

    public E peek(){
        if(queue.isEmpty()){
            return null;
        }else {
            return queue.get(0);
        }
    }

    public E poll(){
        if(queue.isEmpty()){
            return null;
        }else {
            E o =  queue.get(0);
            queue.remove(0);
            return o;
        }
    }

    public boolean remove(Object o){
        return queue.remove(o);
    }

    public int size(){
        return queue.size();
    }

    public ArrayList<E> getArray(){
        return queue;
    }

    private void siftUp(int k, E x) {
        if (comparator != null)
            siftUpUsingComparator(k, x);
        else
            siftUpComparable(k, x);
    }

    private void siftUpComparable(int k, E x) {
        Comparable<? super E> key = (Comparable<? super E>) x;
        while (k > 0) {
            int parent = (k - 1) >>> 1;
            Object e = queue.get(parent);
            if (key.compareTo((E) e) >= 0)
                break;
            queue.add(k, (E) e);
            k = parent;
        }
        queue.add(k, (E) key);
    }

    private void siftUpUsingComparator(int k, E x) {
        while (k > 0) {
            int parent = (k - 1) >>> 1;
            Object e = queue.get(parent);
            if (comparator.compare(x, (E) e) >= 0)
                break;
            queue.add(k, (E) e);
            k = parent;
        }
        queue.add(k, x);
    }

    private class IteratorQueue implements Iterator<E>{
        private int index = 0;
        @Override
        public boolean hasNext() {
            return queue.get(index).equals(null) ? false : true;
        }

        @Override
        public E next() {
            return queue.get(index++);
        }

        @Override
        public void remove() {
            queue.remove(index);
        }
    }
}
