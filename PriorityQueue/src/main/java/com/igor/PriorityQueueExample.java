package com.igor;

import java.util.Random;
import java.util.Scanner;

public class PriorityQueueExample {

    public static void main(String[] args) {
        System.out.println("Example work with Integers\n");
        PriorityQueue<Integer> integerPriorityQueue = new PriorityQueue<>(7);
        Random rand = new Random();
        for (int i = 0; i < 7; i++) {
            integerPriorityQueue.add(rand.nextInt(100));
        }
        for (int i = 0; i < 7; i++) {
            Integer in = integerPriorityQueue.poll();
            System.out.println("Work with Integer:" + in);
        }
        System.out.println("Example work with Person\n");

        PriorityQueue<Person> personPriorityQueue = new PriorityQueue<>(3);
        Scanner scanner = new Scanner(System.in);
        String name, address;
        int age, phone;
        boolean gender;
        for (int i = 0; i < 3; i++) {
            System.out.println("Enter name:");
            name = scanner.nextLine();
            System.out.println("Enter address:");
            address = scanner.nextLine();
            System.out.println("Enter age:");
            age = scanner.nextInt();
            System.out.println("Enter phone");
            phone = scanner.nextInt();
            System.out.println("Enter genger(true-man, false-woman):");
            gender = scanner.nextBoolean();
            personPriorityQueue.add(new Person(name, address, age, phone, gender));
        }
        for (int i = 0; i < 3; i++) {
            Person in = personPriorityQueue.poll();
            System.out.println("Work with class Person:" + in);
        }
    }
}